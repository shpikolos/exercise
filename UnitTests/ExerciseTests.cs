﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exercise;

namespace UnitTests
{
	[TestClass]
	public class ExerciseTests
	{
		[TestMethod]
		public void TestSortMethod()
		{
			Cart[] cartsForTesting = GetUnSortedCarts();

			Exercise.Exercise.Sort(ref cartsForTesting);

			if (!CheckCarts(cartsForTesting))
				Assert.Fail("Ошибка");
		}
		/// <summary>
		/// Возвращает неотсортированные карты.
		/// </summary>
		/// <returns></returns>
		private Cart[] GetUnSortedCarts()
		{
			Cart cart0 = new Cart() { Current = 0, Next = 1 };
			Cart cart1 = new Cart() { Current = 1, Next = 2 };
			Cart cart2 = new Cart() { Current = 2, Next = 3 };
			Cart cart3 = new Cart() { Current = 3, Next = 4 };
			Cart cart4 = new Cart() { Current = 4, Next = 5 };
			Cart cart5 = new Cart() { Current = 5, Next = 6 };

			//	Создание маршрутов.
			Cart[] carts = new Cart[] { cart0, cart1, cart2, cart3, cart4, cart5 };

			//	Перемешивание маршрутов.
			int maxCount = 100;
			Random rnd1 = new Random(DateTime.Now.Millisecond);
			Random rnd2 = new Random(DateTime.Now.Millisecond + 1);
			for (int i = 0; i < maxCount; i++)
			{
				int i1 = rnd1.Next(0, carts.Length);
				int i2 = rnd2.Next(0, carts.Length);
				Cart c = carts[i1];
				carts[i1] = carts[i2];
				carts[i2] = c;
			}
			return carts;
		}
		/// <summary>
		/// Проверяет отсортированность карт.
		/// </summary>
		/// <param name="carts">Карты для проверки.</param>
		/// <returns></returns>
		private Boolean CheckCarts(Cart[] carts)
		{
			for (int i = 0; i < carts.Length - 1; i++)
				if (carts[i].Next != carts[i + 1].Current)
					return false;
			return true;
		}
	}
}
