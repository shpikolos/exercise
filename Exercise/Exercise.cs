﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise
{
	public class Exercise
	{
		/// <summary>
		/// Сортирует элементы, чтобы получился непрерывный маршрут без циклов.
		/// </summary>
		/// <param name="carts">Список элементов для сортировки.</param>
		public static void Sort(ref Cart[] carts)
		{
			for (int i = 1; i < carts.Length; i++)
			{
				for (int j = 0; j < i; j++)
				{
					//	Проверяется можно ли вставить i-й элемент левее от j-го.
					if (carts[j].Current == carts[i].Next)
					{
						//	Перед вставкой i-го элемента необходимо все элементы от j-го до i-го сместить вправо.
						Cart savedCart = carts[i];
						for (int z = i; z > j; z--)
						{
							carts[z] = carts[z - 1];
						}
						carts[j] = savedCart;
						i = 1;
						j = 0;
					}
				}

			}
		}
	}
	/// <summary>
	/// Элемент маршрута.
	/// </summary>
	public class Cart
	{
		/// <summary>
		/// Текущая остановка.
		/// </summary>
		public int Current { get; set; }
		/// <summary>
		///	Следующая остановка.
		/// </summary>
		public int Next { get; set; }
	}
}
